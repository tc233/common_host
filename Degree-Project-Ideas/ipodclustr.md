# iPodClustr

**iPodClustr** is another potential choice for my degree project. The goal is to first find out a way to establish a network connection between multiple iPods, presumably 8 (2x4) or 10 (2x5) and then combine them together to form a larger screen. By selecting an image from the master device, the combined screen can hence display the image separately on each iPod's screen, which create some interesting effects.

## Idea

<div>
    <img style="float: right" align="right" src="/Degree-Project-Ideas/images/iPods.jpg" title="iPod cluster" width="360">
</div>

- During the summer Prof. Ric Telford came up with this idea to make use of those old iPod touch.

- After that, I did some researches online and found a [similar project](https://www.youtube.com/watch?v=VAKhftX0cjk) done many years ago, but was never put into real practice.

- First I want to setup a back-end server to deal with image processing and dispatch the cropped image pieces, but then I leaned toward an ad-hoc network without a central server.

- What other usage can this project provide? Since the main goal seems to be just a toy project, I start to think about other usage for it. Then I recalled the [`Potato Passing Game`](http://people.duke.edu/~tkb13/courses/ece650-2018sp/homeworks/homework3-program.pdf) in **ECE 650 - System Programming** course, where students were instructed to try with socket programming and see how to maintain a connection between nodes. Maybe I can create a UI version for it.

- More stuff such as a simple distributed system might be designed and implemented with this network.

## Architecture and System design

<div>
	<img src="/Degree-Project-Ideas/images/potato-network.png" title="potato-network" width="360">
	<img src="/Degree-Project-Ideas/images/master-slave-network.png" title="master-slave-network" >
</div>

1. The potato network example in ECE 650 homework.
2. The master-slave network needed for image dispatch and display.

Basically, both models of networks requires a master device to send out data/instructions to clients, and let clients to do different jobs. 

### Problems to solve

To establish a network between iPods, and do miscellaneous jobs on each device.

### Needed parts

#### hardware
 
- Multi-iPod with 3D printed frame as their fixture

Design a frame to attach 8 or 10 iPods on a tablet in order.

- A master device

#### software

![Verify the idea](/Degree-Project-Ideas/videos/transfer.mov)

- Start with 3+1 devices and establish a network
test to see if the network is stable

- Image/video chunking

  - pick an image from master's album

  - Master-slave paradigm - the master device loads an image, crop and chunk it and then send to all iPods

  - Enumerate all possible cut modes from 2 device to 12 devices, and pick the mode when master device loads

- Sync all slave devices when network is established

By sending a struct with master device's timestamp and the time to start the action together with the data/signal, the clients can decide when to invoke the action in a near future. Then the difference between itself and master's clock is saved in the memory, until another sync signal is received to re-calibrate their times.

- Game of potato  

  UI version of ECE650 Potato Passing Game. Send a potato to a random iPod, then the potato is passed to another iPod every time interval.

  - When an iPod is idling, it displays its device id.

  - When an iPod receive the potato, it will display a potato image on the screen, together with remaining counts and previous paths. As soon as the count reaches zero, the potato is sent back to master, and the master could print out the path.

## Highlights

- Socket programming/application on iOS devices

- Time syncing algorithm implementation

- Image processing

## Pros and Cons

### pros
- fairly complex, no existing app or source code does this
- can learn many new things of mobile programming together with network programming
- show off skills, fancy and attractive
- learn about distributed system in another sense
- have many options to do it, results could vary
- devise a multi device synchronization mechanism

### cons
- not really useful  
the app has limited using scenarios, and most possibly ends up as a toy project
- no practical  
at most 2 x 5 = 10 iPods can line up together due to their wide black border for front camera and round button
- architecture is simple  
no server side architecture, all logics are running in a [ad-hoc](https://en.wikipedia.org/wiki/Wireless_ad_hoc_network) fashion
- similar algorithms might already exists in other industry, such as outdoor advertising with large stitched screens

![links](/Degree-Project-Ideas/images/links.jpg)

I saw there is a TV wall in Duke Library links. It seems that joint screens are already a mature tech and it is a bit trivial to invent that again.

Therefore, my implementation might be naive and simple, and does not have enough value.

## Roadmap

1. establish a test network on 3+1 devices
2. try image chunking with CoreImage framework. If it does not perform well, write C program to deal with image matrix
3. design the prototype of this app, and test its stability
4. UI improvements as well as potato game implementation
5. final tests

---

[Original issue](https://gitlab.oit.duke.edu/MobileCenter/task_queue/issues/8)

