# DukePersonServer

**DukePersonServer** is a TA preparatory project designed for **ECE564 - iOS Mobile Development** in 2019 fall.

Please refer to [ECE564_2019fall](https://gitlab.oit.duke.edu/tc233/ece564_2019fall_prep/wikis/home) for more details.

Also, the current working prototype is on [this server](http://rt113-dt01.egr.duke.edu:5640/). Please connect to Duke campus network to access this address.

This back-end service, together with my previous ECE564 [homework project](https://gitlab.oit.duke.edu/tc233/ECE564_HOMEWORK), form a Client-Server project in whole.

## Idea

- During the summer Prof. Ric Telford started to prepare for ECE564 this fall, and plan to develop a full Swift example project as the pathway for student to get familiar with iOS and Swift programming.

- 3 main Server-Side-Swift frameworks exist, and I chose **Kitura** for its similarity to Django. Then I developed this back-end service during the end of June.

## Architecture and System design

![links](/Degree-Project-Ideas/images/bff-overview.jpg)
Adapted from Design pattern: [Backends For Frontends](https://samnewman.io/patterns/architectural/bff/)

The back-end follows the suggested Model-View-Controller (MVC) design pattern. 
- Views are rendered with Stencil template engine, together with front-end JavaScript functions and stylesheets.
- Controller/Routes decide how to route a request and what is the response
- Models define data structures to interact with relations in database
- More can be found in the [project description wiki](https://gitlab.oit.duke.edu/tc233/ece564_2019fall_prep/wikis/home).

## Pros and Cons

### pros
- Not time consuming for further improvements

  I know about the whole architecture clearly, and it is easier to add new features of make improvements.

- Already done most of the job for the course
- Typical sample project for education

  After the semester ends, this project can be packed up as a sample project for education, a tutorial or a template project, for people to set up a lightweight HTTP service with many handy features in Swift. It will facilitate the process as many functions can be re-used with little modification.

- A clear architecture
  
  All of its features are implemented within Swift, which is good as a Server-Side-Swift example

- More features can be added

  Such as cache and ajax CORS middleware. More features can make this service a more complete management system.

  Also, with the release of SwiftUI in WWDC 2019, even the front-end logics can take advantage of Swift! Use [SwiftWebUI](https://github.com/swiftwebui/SwiftWebUI) to replace ajax API calls and other js stuff, HTML elements.

  Add Redis or Memchached to enhance query speed.

### cons

- Not innovative

   - Not challenging for Graduate level
   - The workload is not equivalent to the bar of a higher degree

- Ordinary

  It does not provide any fascinating features or insightful results. Just a rewrite of existing features in a different language.

- Cannot learn much from it

  I don't like to waste time on what I already know quite well.

## Roadmap

Main implementation done within June 24-29.
